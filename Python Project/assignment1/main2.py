import sys
from PyQt5.QtWidgets import QWidget,QApplication,QLabel,QPushButton

class App(QWidget):
    def __init__(self):
       super().__init__()
       self.left=50
       self.top=640
       self.height=800
       self.width=480
       self.button()
       
    def button(self):
            self.label=QLabel(self)
        
            self.label.setText("hii how are you!")
            self.label.move(100,30)
            btn=QPushButton("pyqt button",self)
            #This line creates a QPushButton with the label "pyqt button" and sets its parent widget to self. self is typically a reference to the parent widget (e.g., a QMainWindow or another QWidget).
            btn.setToolTip("Please click the button")
             #This line sets a tooltip for the button. The tooltip is the text that appears when you hover the mouse over the button.
            btn.move(100,70)#This line moves the button to the position (100, 70) within its parent 

            btn.clicked.connect(self.on_click)
            self.show()
    def on_click(self):
         self.label.setText("button clicked")

if __name__=="__main__":
    app=QApplication(sys.argv)
    ex=App()
    sys.exit(app.exec_())

