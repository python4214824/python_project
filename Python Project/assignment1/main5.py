import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QColor
class demo(QMainWindow,QWidget):
    def __init__(self):
        super().__init__()
        self.setGeometry(100,100,500,600)
        self.color()
    def color(self):
        box=QComboBox(self)
        box.addItems(["Set color","red","green","yellow","magenta","pink"])
        box.currentIndexChanged.connect(self.chngcolor)
        box.setCurrentIndex(0)
    def chngcolor(self,indx):
        color_name=self.sender().currentText()if self.sender() else "select color"
        if color_name != "Set color":
            
            color = QColor(color_name)
            self.setStyleSheet(f"background-color:{color.name()};")
      
        
if __name__=="__main__":
    app=QApplication(sys.argv)
    ex=demo()
    ex.show()
    app.exit(app.exec_())