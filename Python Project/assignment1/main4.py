import sys
from PyQt5.QtWidgets  import QApplication,QMainWindow,QLabel,QMessageBox,QLineEdit,QWidget,QPushButton
class demo(QMainWindow,QWidget):
    def __init__(self):
        super().__init__()
        self.setGeometry(200,100,800,500)
        self.dialogbox()
        
        
    def dialogbox(self):
        label=QLabel("Enter your Name",self)
        label.setGeometry(250,20,400,30)
        
        
        self.line_edit = QLineEdit(self)
        self.line_edit.setGeometry(100,50,400,30)
        btn=QPushButton("OK",self)
        btn.setGeometry(100,90,400,30)
        btn.clicked.connect(self.on_click_ok)
        self.show()
        
        
        
    def on_click_ok(self):
        input_text = self.line_edit.text()
        QMessageBox.information(self, "My Name",input_text)

if __name__=="__main__":
    app=QApplication(sys.argv)
    ex=demo()
    sys.exit(app.exec_())