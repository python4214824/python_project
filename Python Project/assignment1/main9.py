import sys
from PyQt5.QtWidgets import QApplication,QLabel,QVBoxLayout,QWidget,QProgressBar,QMainWindow,QPushButton
from PyQt5.QtGui import QPixmap
import urllib.request
class demo(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setGeometry(100, 100, 600, 400)
        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)
        self.pbar()
    def pbar(self):
        self.probar=QProgressBar(self)
        self.probar.setGeometry(25, 45, 210, 30)

        self.btn=QPushButton("start",self)
        self.btn.move(50, 100)
        
        self.btn.clicked.connect(self.Download)
    def Handle_Progress(self, blocknum, blocksize, totalsize):
## calculate the progress
        readed_data = blocknum * blocksize
        if totalsize > 0:
            download_percentage = readed_data * 100 / totalsize
            self.probar.setValue(int(download_percentage))
            QApplication.processEvents()

    def Download(self):
        down_url ='https://img.indiaforums.com/person/640x480/1/2525-prabhas.jpg?c=4bM0D6'
        save_loc ="C://Users//Akanksha//Desktop//Python Project//assignment1//prabhas.jpg"
        urllib.request.urlretrieve(down_url,save_loc, self.Handle_Progress)
        

if __name__=="__main__":
    app=QApplication(sys.argv)
    ex=demo()
    ex.show()
    sys.exit(app.exec_())