import sys
from PyQt5.QtWidgets import QWidget,QApplication,QLabel,QMenuBar,QMenu,QAction, QMainWindow,QMessageBox
class demo(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setGeometry(200,200,800,800)
        self.nav()
    def nav(self):
        menu=self.menuBar()

        file_menu=QMenu("File",self)
        menu.addMenu(file_menu)

        open=QAction("Open",self)
        file_menu.addAction(open)
        open.triggered.connect(self.openact)

        exit=QAction("Exit",self)
        file_menu.addAction(exit)
        exit.triggered.connect(self.exitact)

        edit_menu=QMenu("Edit",self)
        menu.addMenu(edit_menu)

        copy=QAction("Copy",self)
        edit_menu.addAction(copy)
        copy.triggered.connect(self.copyact)
       
        self.show()

    def openact(self):
        QMessageBox.information(self, "Open Action", "Open action triggered.")

    def exitact(self):
        QMessageBox.information(self, "exit Action", "exit action triggered.")

    def copyact(self):
        QMessageBox.information(self, "Copy Action", "Copy action triggered.")

if __name__=="__main__":
    app=QApplication(sys.argv)
    ex=demo()
    sys.exit(app.exec_())


