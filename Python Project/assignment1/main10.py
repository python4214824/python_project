import sys
from PyQt5.QtWidgets import QApplication,QLabel,QVBoxLayout,QWidget,QTabWidget,QMainWindow,QPushButton
from PyQt5.QtGui import QPixmap
class demo(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setGeometry(100, 100, 600, 400)
        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)
        self.tab()
    def tab(self):
        tab_widget = QTabWidget()
        tab1=QWidget()
        tab2= QWidget()

        layout1 = QVBoxLayout(tab1)
        layout2 = QVBoxLayout(tab2)

        btn=QPushButton("CLICK ME!")
       
        btn.setFixedSize(80, 30)         
        layout1.addWidget(btn)
        image_label = QLabel()
        pixmap = QPixmap("C://Users//Akanksha//Desktop//Python Project//assignment1//logo.png")
        image_label.setPixmap(pixmap)
        layout2.addWidget(image_label)

        tab_widget.addTab(tab1, "Tab 1")
        tab_widget.addTab(tab2, "Tab 2")
    
        layout = QVBoxLayout(self.central_widget)
        layout.addWidget(tab_widget)

        

if __name__=="__main__":
    app=QApplication(sys.argv)
    ex=demo()
    ex.show()
    sys.exit(app.exec_())