import sys
from PyQt5.QtWidgets import QWidget,QApplication,QLabel,QVBoxLayout,QTableWidget,QTableWidgetItem,QMainWindow
class demo(QMainWindow):
    def __init__(self):
            super().__init__()
            self.setGeometry(100, 100, 600, 400)
           
            self.central_Widget=QWidget()
            self.setCentralWidget(self.central_Widget)
            self.createtable()

    def createtable(self):
          table=QTableWidget()
          table.setRowCount(3)
          table.setColumnCount(3)
          table.setHorizontalHeaderLabels(["Column 1", "Column 2", "Column 3"])
          table.setItem(0, 0, QTableWidgetItem("Data 1"))
          table.setItem(0, 1, QTableWidgetItem("Data 2"))
          table.setItem(0, 2, QTableWidgetItem ("Data3"))
          layout=QVBoxLayout(self.central_Widget)
          layout.addWidget(table)
    

if __name__=="__main__":
      app=QApplication(sys.argv)
      ex=demo()
      ex.show()
      sys.exit(app.exec_())