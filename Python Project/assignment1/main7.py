import sys
from PyQt5.QtWidgets import QApplication,QWidget,QVBoxLayout,QSlider,QLabel
from PyQt5.QtCore import Qt
class demo(QWidget):
    def __init__(self):
        super().__init__()
        self.slider()
    def slider(self):
        layout = QVBoxLayout()
        sld=QSlider(Qt.Horizontal)
        
        sld.setMinimum(0)
        sld.setMaximum(100)
        self.label = QLabel('Selected value: 0')
        layout.addWidget(sld)
        layout.addWidget(self.label)
        self.setLayout(layout)
        self.setGeometry(300, 300, 300, 200)
        sld.valueChanged.connect(self.onclick)
    def onclick(self,value):
        self.label.setText(f'Selected value: {value}')


        
if __name__=="__main__":
    app=QApplication(sys.argv)
    ex=demo()
    ex.show()
    sys.exit(app.exec_())
 