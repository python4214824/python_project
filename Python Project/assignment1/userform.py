import sys
from PyQt5.QtWidgets import QWidget,QApplication,QMessageBox,QLineEdit,QLabel,QVBoxLayout,QPushButton,QRadioButton,QFileDialog
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap

class demo(QWidget):
    def __init__(self):
        super().__init__()
        self.setGeometry(100, 100, 850, 800)
        self.userform()
    def userform(self):
        layout=QVBoxLayout()
    
        
        label=QLabel("User Profile Form",self)
        label.setGeometry(10,10,780,60)
        label.setStyleSheet("background-color:blue ; color:white")
        label.setAlignment(Qt.AlignCenter)
        font = label.font()
        font.setPointSize(font.pointSize() + 6)  # Increase the size as needed
        label.setFont(font)
       
        

       # heading_label.setStyleSheet('background-color: lightblue; color: darkred;')

        self.label1 = QLabel('Enter First Name :',self).setGeometry(10,80,150,40)
        self.line_edit1 = QLineEdit(self)
        self.line_edit1.setGeometry(160,80,630,30)
    


        self.label2 = QLabel('Enter Last Name :',self).setGeometry(10,120,150,40)
        self.line_edit2 = QLineEdit(self)
        self.line_edit2.setGeometry(160,120,630,30)

        self.label3 = QLabel('Enter Mobile No. :',self).setGeometry(10,160,150,40)
        self.line_edit3 = QLineEdit(self)
        self.line_edit3.setGeometry(160,160,630,30)

        self.label4 = QLabel('Select Profile Photo : No file Selected',self).setGeometry(10,200,250,40)
       
    
        btn1=QPushButton("Select Photo",self)
    
        btn1.setGeometry(10,240,780,30)
        
        label5 = QLabel('Gender :',self).setGeometry(10,280,100,40)
        self.btn2=QRadioButton("Male",self).setGeometry(120,280,100,40)
        self.btn3=QRadioButton("Female",self).setGeometry(120,320,100,40)
        
        self.label5 = QLabel('Height(cm) :').setGeometry(10,360,250,40)
        self.line_edit4 = QLineEdit(self)
        self.line_edit4.setGeometry(160,360,630,30)

        btn4=QPushButton("Submit",self)
        btn4.setGeometry(10,400,780,40)


        self.photo_label = QLabel(self)
        self.photo_label.setAlignment(Qt.AlignCenter)
        self.photo_label.setPixmap(QPixmap('placeholder_image.png'))
        btn1.clicked.connect(self.showFileDialog)
        btn4.clicked.connect(self.on_click_submit),self
    def showFileDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog  # Ensure consistent behavior across platforms

        fileName, _ = QFileDialog.getOpenFileName(self, "Select Photo", "", "Image Files (*.png *.jpg *.jpeg *.bmp *.gif);;All Files (*)", options=options)
    def on_click_submit(self):
        input_text1 = self.line_edit1.text()
        input_text2 = self.line_edit2.text()
        input_text3 = self.line_edit3.text()
        input_text4 = self.line_edit3.text()
        
        QMessageBox.information(self, "Name: {} {} \n Mobile: {}\n Height:{}\n ".format(input_text1,input_text2,input_text3,input_text4))



if __name__=="__main__":
    app=QApplication(sys.argv)
    ex=demo()
    ex.show()
    sys.exit(app.exec_())
