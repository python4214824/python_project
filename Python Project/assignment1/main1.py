import sys
from PyQt5.QtWidgets import QApplication,QWidget,QLabel,QVBoxLayout
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import Qt

class App(QWidget):
    def __init__(self):
        super().__init__()
        self.left=50
        self.top=640
        self.height=90
        self.width=480
        self.title="first app"
        self.mainLayout=QVBoxLayout(self)
        self.text()
        self.initUI()
       
    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left,self.top,self.width,self.height)
        self.show()
    def text(self):
        self.label=QLabel(self)
        self.label.setText("Hello PyQt!")
        self.label.setAlignment(Qt.AlignCenter)
       
        
                #self.mainLayout.addWidget(self.label,0,Qt.AlignmentFlag.AlignCenter)
        
if __name__=="__main__":
    app=QApplication(sys.argv)
    ex=App()
    sys.exit(app.exec_())
