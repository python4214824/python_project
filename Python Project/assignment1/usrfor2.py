import sys
from PyQt5.QtWidgets import QWidget, QApplication, QLabel, QLineEdit, QVBoxLayout, QPushButton, QRadioButton, QFileDialog
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap

class Demo(QWidget):
    def __init__(self):
        super().__init__()
        self.setGeometry(100, 100, 850, 800)
        self.userform()

    def userform(self):
        layout = QVBoxLayout()

        label = QLabel("User Profile Form", self)
        label.setStyleSheet("background-color:blue ; color:white")
        label.setAlignment(Qt.AlignCenter)
        font = label.font()
        font.setPointSize(font.pointSize() + 6)
        label.setFont(font)

        self.label1 = QLabel('Enter First Name :', self)
        self.line_edit1 = QLineEdit(self)

        self.label2 = QLabel('Enter Last Name :', self)
        self.line_edit2 = QLineEdit(self)

        self.label3 = QLabel('Enter Mobile No. :', self)
        self.line_edit3 = QLineEdit(self)

        self.label4 = QLabel('Select Profile Photo : No file Selected', self)
        layout.addWidget(self.label4)

        btn1 = QPushButton("Select Photo", self)
        btn1.setGeometry(10, 240, 780, 30)
        layout.addWidget(btn1)

        label5 = QLabel('Gender :', self)
        self.btn2 = QRadioButton("Male", self)
        self.btn3 = QRadioButton("Female", self)

        self.label5 = QLabel('Height(cm) :', self)
        self.line_edit4 = QLineEdit(self)

        btn4 = QPushButton("Submit", self)
        btn4.setGeometry(10, 400, 780, 40)
        layout.addWidget(btn4)

        self.photo_label = QLabel(self)
        self.photo_label.setAlignment(Qt.AlignCenter)
        self.photo_label.setPixmap(QPixmap('placeholder_image.png'))
        layout.addWidget(self.photo_label)

        btn1.clicked.connect(self.showFileDialog)
        btn4.clicked.connect(self.on_click_submit)

        self.setLayout(layout)

    def showFileDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self, "Select Photo", "", "Image Files (*.png *.jpg *.jpeg *.bmp *.gif);;All Files (*)", options=options)

    def on_click_submit(self):
        input_text1 = self.line_edit1.text()
        input_text2 = self.line_edit2.text()
        input_text3 = self.line_edit3.text()
        input_text4 = self.line_edit4.text()
        print("Name: {} {}\nMobile: {}\nHeight: {}".format(input_text1, input_text2, input_text3, input_text4))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = Demo()
    ex.show()
    sys.exit(app.exec_())

