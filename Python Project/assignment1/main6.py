import sys 
from PyQt5.QtWidgets import QWidget,QVBoxLayout,QApplication,QRadioButton,QLabel
class demo(QWidget):
    def __init__(self):
        super().__init__()
        self.radiobtn()
    def radiobtn(self):
        layout=QVBoxLayout()
        self.btn1=QRadioButton("Python")
        self.btn2=QRadioButton("CPP")
        self.btn3=QRadioButton("JAVA")
        layout.addWidget(self.btn1)
        layout.addWidget(self.btn2)
        layout.addWidget(self.btn3)
        self.setLayout(layout) 
        self.setGeometry(300, 300, 300, 200)
        self.label=QLabel("Selected option: None",self)
        layout.addWidget(self.label)
        
        self.btn1.toggled.connect(lambda :self.on_click("Python"))
        self.btn2.toggled.connect(lambda:self.on_click("CPP"))
        self.btn3.toggled.connect(lambda:self.on_click("JAVA"))
        self.setLayout(layout)
    def on_click(self,option):
        
        if option:
         self.label.setText( f"Selected option: {option}")


if __name__=="__main__" :
    app=QApplication(sys.argv)
    ex=demo()
    ex.show()
    sys.exit(app.exec_())