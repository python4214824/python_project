import sys
from PyQt5.QtWidgets import QWidget,QApplication,QVBoxLayout,QLabel
from PyQt5.QtCore import Qt
class mainwindow(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("main window")
        self.setGeometry(500,300,500,300)
        self.mainLayout=QVBoxLayout(self)
        self.setLayout(self.mainLayout)
        self.labelui()
    def labelui(self):
        self.label=QLabel("hello core2web")
        self.label.setStyleSheet("background-color:cyan")
       
        self.mainLayout.addWidget(self.label,0,Qt.AlignmentFlag.AlignCenter)
if __name__=="__main__":
    app=QApplication(sys.argv)
    main_window=mainwindow()
    main_window.show()
    sys.exit(app.exec_())
