import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from Home import C2W_Home;


class Widget(QWidget,C2W_Home):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.c2w_ui = C2W_Home()
        self.c2w_ui.setupUi(self)

# class MainWindow(QWidget):
#     def __init__(self):
#         super().__init__()
#         self.setWindowTitle("Main Window")
#         self.setGeometry(900,600,900,600)
#         self.setStyleSheet("background-color: lightblue;")
#         self.mainLayout = QVBoxLayout(self)
#         self.setLayout(self.mainLayout)
#         self.LabelUI()

#     def LabelUI(self):
#         self.label = QLabel("Hello Core2Web")
#         self.mainLayout.addWidget(self.label, 0, Qt.AlignmentFlag.AlignCenter)



if __name__ == "__main__":
    app = QApplication(sys.argv)
    widget = Widget()
    widget.show()
    # main_Window = MainWindow()
    # main_Window.show()
    sys.exit(app.exec_())